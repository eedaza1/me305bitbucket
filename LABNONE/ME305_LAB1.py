# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
"The function below will calculate the index of the fibonacci sequence based on"    
"the desired index given by the user "
def fib (x):
    
    "The first two numbers of the sequence are introduce to allow for"
    "the mathematics below to be able to occur"
    
    fibnum = [0, 1]
    
    "The first two number are automatically output as done by the "
    "conditional system below"
    if x==0:
        print(0)
    elif x==1:
        print(1)
        "The rest of the values are calculated below"
    else:
        for k in range(1,x):
            "The loop will add the current value K in the sequence and the previous"
            "value in the sequence"
            fibnum.append(fibnum[k]+fibnum[k-1])
            "The conditional statement below will allow for the fibonacci sequence"
            "index to be printed"
            if k != x-1:
                continue
            else:
                print(fibnum[x])

"The code below will avoid the code from being executed when imported"                
if __name__ == '__main__':
  
 "The user is asked to input a value for an index below "
 x=input('Enter Number a Whole Number: ')
 "The continue_or_end variable below allows for the while loop to continue"
 "until otherwise indicated to"
 Continue_or_End='Continue'
 "The while loop will continue to run until terminated by user "
 while Continue_or_End == 'Continue':
     "Checking if the use input is of the correct data type"
     if x.isdigit():
        "Converting the input value into an integer"
        x=int(x) 
        "The code below will calculate the fibonacci number with the function above"
        print('Fibonaci number at index '+str(x)+' is: ')
        y=fib(x)
        try:
            "This series of code will allow the user to either continue or exit"
            Y_N=float(input('Do you want to continue ? Press 0 to end, enter anything else to continue: '))
            if Y_N == 0:
                 "This conditional statement will end the loop"
                 Continue_or_End='End'
            else:
                 "This conditional statement will continue the loop"
                 Continue_or_End='Continue'
                 x=input('Enter Number a Whole Number: ')   
        except ValueError:
            ""
            x=input('Enter Number a Whole Number: ') 
     else:
         "This code is executed if the user does not input a valid data type"
         x=input('Please Enter a whole number: ')
         Continue_or_End='Continue'
         
# Memo:
#     The first part of the code above defines the function that will be used. The
#     way that the function works is that it already has the first two numbers
#     in the sequence. This is because the way that it functions is it adds the nth
#     value and the (n-1) value to yield the (n+1) value. Therefore it needs at least 
#     2 initial values to continually loop and add up to the desired indexed value. 
#     If either  one of the first two fibonacci sequence values are desired, it will 
#     not do any calculations. Rather it will automatically just output either the 
#     first or second value of the sequence. 
    
#     The while loop works in the following manner. It will first check if the input
#     value is of valid data type, which is to say, it will check if the input value
#     is a whole number. If it isnt it will prompt the user to re-input a value of valid
#     data type. Once the data type of the proper type is input, it will then call
#     the function described above. It will then prompt the user to decide whether
#     or not to continue the program. If the user inputs 0, it will end the program,
#     if the user inputs anything other than zero it will continue to loop through     
#     the program until the user inputs 0. It does have a try and except condition
#     and that is in place because there was a lingering error that would halt 
#     the loop. 